public interface RomanNumeralsConverter {

    /**
     * Converts the input decimal number to the Roman equivalent. Supported are numbers in range 1-3999
     * Examples: 1->I, 4->IV, 27->XXVII, 416->CDXVI, 899->DCCCXCIX,  3999 -> MMMCMXCIX
     * @param decimalNumber Positive decimal number.
     * @return Roman number equivalent to the decimal input.
     * @throws IllegalArgumentException if the argument is out of range.
     */
    String convert(int decimalNumber);
}
