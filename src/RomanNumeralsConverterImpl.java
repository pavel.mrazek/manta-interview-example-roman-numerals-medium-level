import java.util.Optional;

public class RomanNumeralsConverterImpl implements RomanNumeralsConverter {

    /** Lowest decimal number that can be converted into Roman number. */
    private static final int MIN_VALUE = 1;

    /** Currently highest supported decimal number that can be converted into Roman number. */
    private static final int MAX_VALUE = 3999;


    @Override
    public String convert(int decimalNumber) {
        // TODO implement this method using the already existing private methods (they work just fine, no need to modify them)
        return null;
    }


    /**
     * Returns exactly matching Roman symbol for the input decimal value. Or empty optional if such Roman symbol is not defined.
     * Examples:   1 -> I,  III -> undefined,   5 -> V,   37 -> undefined,    100 -> C
     * @param value Input decimal value to search for the matching Roman symbol
     */
    private Optional<RomanNumeralOrder> getExactMatchRomanOrder(int value){
        Optional<RomanNumeralOrder> romanOrder = Optional.of(RomanNumeralOrder.getHighestOrder());
        while(romanOrder.isPresent() && romanOrder.get().getDecimalValue()!=value){
            romanOrder = romanOrder.get().getLowerOrder();
        }
        return romanOrder;
    }


    /**
     * Returns maximal lower Roman symbol for the input decimal value. Or empty optional if such Roman symbol is not defined.
     * Examples:   1 -> undefined,   5 -> I,   30 -> X,    101 -> C
     * @param value Input decimal value to search for the maximal lower corresponding Roman symbol
     */
    private Optional<RomanNumeralOrder> getMaxLowerRomanOrder(int value){
        Optional<RomanNumeralOrder> romanOrder = Optional.of(RomanNumeralOrder.getHighestOrder());
        while(romanOrder.isPresent() && romanOrder.get().getDecimalValue()>=value){
            romanOrder = romanOrder.get().getLowerOrder();
        }
        return romanOrder;
    }


    /**
     * Returns minimal higher Roman symbol for the input decimal value. Or empty optional if such Roman symbol is not defined.
     * Examples:   1 -> V,   5 -> X,   30 -> L,    101 -> D
     * @param value Input decimal value to search for the minimal higher corresponding Roman symbol
     */
    private Optional<RomanNumeralOrder> getMinHigherRomanOrder(int value){
        RomanNumeralOrder romanOrder = RomanNumeralOrder.I;
        while(romanOrder.getDecimalValue()<=value && romanOrder.getHigherOrder().isPresent()){
            romanOrder = romanOrder.getHigherOrder().get();
        }

        return Optional.of(romanOrder);
    }


    /**
     * Converts the decimalNumber to String.
     */
    private String toString(int decimalNumber) {
        return Integer.toString(decimalNumber);
    }


    /**
     * Returns the net value of the decimalNumber at the given position. Previous positions are ignored
     * and following positions are considered zero.
     *
     * Examples: num=148, pos=0 -> returns 100,    num=148, pos=1 -> returns 40,       num=148, pos=2 -> returns 8
     * @param decimalNumber String representation of the decimal number
     * @param pos index of the character in the input string to be converted into a digit
     */
    private int getNetValueAtPosition(String decimalNumber, int pos) {
        final int digitAtPos = getDigitAtPos(decimalNumber, pos);
        final int order = decimalNumber.length() - pos - 1;
        final int orderPow10 = (int) Math.pow(10, order);
        return  digitAtPos * orderPow10;
    }


    /**
     * Returns the decimalNumber's digit at the required position.
     * Example: num=148, pos=1 -> returns 4
     * @param decimalNumberStr String representation of the decimal number
     * @param pos index of the character in the input string to be converted into a digit
     * @throws IllegalArgumentException if the position is out of bounds (larger then the stringified number length)
     */
    private int getDigitAtPos(String decimalNumberStr, int pos) {
        if(pos<0 || pos>=decimalNumberStr.length()){
            throw new IllegalArgumentException("Cannot get digit for illegal position " + pos + " in number " + decimalNumberStr);
        }
        final char ch = decimalNumberStr.charAt(pos);
        return ch - '0';     // convert char to int value (using position of the char in ASCII table)
    }

}
