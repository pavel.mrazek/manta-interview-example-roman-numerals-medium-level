import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

public class RomanNumeralsConverterTest {

    final static RomanNumeralsConverter converter = new RomanNumeralsConverterImpl();
    final static List<Pair<Integer, String>> results = new ArrayList<>();

    @BeforeClass
    public static void setup(){
        results.add(new Pair<>(1,  "I"));
        results.add(new Pair<>(2,  "II"));
        results.add(new Pair<>(3,  "III"));
        results.add(new Pair<>(4,  "IV"));
        results.add(new Pair<>(5,  "V"));
        results.add(new Pair<>(6,  "VI"));
        results.add(new Pair<>(7,  "VII"));
        results.add(new Pair<>(8,  "VIII"));
        results.add(new Pair<>(9,  "IX"));
        results.add(new Pair<>(10, "X"));
        results.add(new Pair<>(11, "XI"));
        results.add(new Pair<>(12, "XII"));
        results.add(new Pair<>(13, "XIII"));
        results.add(new Pair<>(14, "XIV"));
        results.add(new Pair<>(15, "XV"));
        results.add(new Pair<>(16, "XVI"));
        results.add(new Pair<>(17, "XVII"));
        results.add(new Pair<>(18, "XVIII"));
        results.add(new Pair<>(19, "XIX"));
        results.add(new Pair<>(20, "XX"));
        results.add(new Pair<>(21, "XXI"));
        results.add(new Pair<>(30, "XXX"));
        results.add(new Pair<>(40, "XL"));
        results.add(new Pair<>(42, "XLII"));
        results.add(new Pair<>(43, "XLIII"));
        results.add(new Pair<>(44, "XLIV"));
        results.add(new Pair<>(45, "XLV"));
        results.add(new Pair<>(46, "XLVI"));
        results.add(new Pair<>(47, "XLVII"));
        results.add(new Pair<>(49, "XLIX"));
        results.add(new Pair<>(50, "L"));
        results.add(new Pair<>(51, "LI"));
        results.add(new Pair<>(52, "LII"));
        results.add(new Pair<>(53, "LIII"));
        results.add(new Pair<>(54, "LIV"));
        results.add(new Pair<>(55, "LV"));
        results.add(new Pair<>(60, "LX"));
        results.add(new Pair<>(70, "LXX"));
        results.add(new Pair<>(80, "LXXX"));
        results.add(new Pair<>(90, "XC"));
        results.add(new Pair<>(95, "XCV"));
        results.add(new Pair<>(97, "XCVII"));
        results.add(new Pair<>(98, "XCVIII"));
        results.add(new Pair<>(99, "XCIX"));
        results.add(new Pair<>(100, "C"));
        results.add(new Pair<>(101, "CI"));
        results.add(new Pair<>(110, "CX"));
        results.add(new Pair<>(127, "CXXVII"));
        results.add(new Pair<>(130, "CXXX"));
        results.add(new Pair<>(150, "CL"));
        results.add(new Pair<>(160, "CLX"));
        results.add(new Pair<>(178, "CLXXVIII"));
        results.add(new Pair<>(180, "CLXXX"));
        results.add(new Pair<>(191, "CXCI"));
        results.add(new Pair<>(192, "CXCII"));
        results.add(new Pair<>(199, "CXCIX"));
        results.add(new Pair<>(200, "CC"));
        results.add(new Pair<>(201, "CCI"));
        results.add(new Pair<>(222, "CCXXII"));
        results.add(new Pair<>(230, "CCXXX"));
        results.add(new Pair<>(248, "CCXLVIII"));
        results.add(new Pair<>(250, "CCL"));
        results.add(new Pair<>(294, "CCXCIV"));
        results.add(new Pair<>(295, "CCXCV"));
        results.add(new Pair<>(299, "CCXCIX"));
        results.add(new Pair<>(300, "CCC"));
        results.add(new Pair<>(333, "CCCXXXIII"));
        results.add(new Pair<>(334, "CCCXXXIV"));
        results.add(new Pair<>(400, "CD"));
        results.add(new Pair<>(485, "CDLXXXV"));
        results.add(new Pair<>(491, "CDXCI"));
        results.add(new Pair<>(499, "CDXCIX"));
        results.add(new Pair<>(500, "D"));
        results.add(new Pair<>(631, "DCXXXI"));
        results.add(new Pair<>(750, "DCCL"));
        results.add(new Pair<>(899, "DCCCXCIX"));
        results.add(new Pair<>(1000, "M"));
        results.add(new Pair<>(1001, "MI"));
        results.add(new Pair<>(1540, "MDXL"));
        results.add(new Pair<>(2089, "MMLXXXIX"));
        results.add(new Pair<>(2500, "MMD"));
        results.add(new Pair<>(2743, "MMDCCXLIII"));
        results.add(new Pair<>(2999, "MMCMXCIX"));
        results.add(new Pair<>(3000, "MMM"));
        results.add(new Pair<>(3756, "MMMDCCLVI"));
        results.add(new Pair<>(3999, "MMMCMXCIX"));
    }


    @Test
    public void test(){
        final StringBuilder errorMessagesBuilder = new StringBuilder();

        // iterate over all expected results
        results.forEach(pair->
            {
                final String actualResult = converter.convert(pair.getKey());
                // in case of unexpected result collect error message
                if(!pair.getValue().equals(actualResult)){
                    errorMessagesBuilder.append("Incorrect conversion: ")
                        .append(pair.getKey())
                        .append("->")
                        .append(pair.getValue())
                        .append(",  got '")
                        .append(actualResult)
                        .append("'")
                        .append(System.lineSeparator());
                }
            }
        );

        // fail if any error message is there
        final String errorMessages = errorMessagesBuilder.toString();
        System.out.println(errorMessages);
        assertTrue(errorMessages.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void isArgumentOutOfRange1(){
        converter.convert(4000);
    }

    @Test(expected = IllegalArgumentException.class)
    public void isArgumentOutOfRange2(){
        converter.convert(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void isArgumentOutOfRange3(){
        converter.convert(-1);
    }



    /** An immutable value class containing a key - value pair such as used in a map. */
    private final static class Pair<K, V> {

        private final K key;

        private final V value;

        private Pair(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }
    }

}
